-- 1
SELECT * FROM artist WHERE name LIKE "%d%";
-- 2
SELECT * FROM songs WHERE length < '00:04:__';
-- 3
SELECT a.album_title, b.name, b.length FROM albums AS a 
JOIN songs AS b ON a.id = b.album_id;
-- 4
SELECT a.id, a.name, b.id, b.album_title, b.date_released, b.artist_id FROM artists AS a 
JOIN albums AS b ON a.id=b.artist_id;
--  5
SELECT * FROM albums ORDER BY album_title DESC LIMIT 4;
-- 6
SELECT * FROM albums AS a 
JOIN songs AS b ON a.id = b.album_id WHERE b.length > '00:03:30_' ORDER BY a.album_title DESC;